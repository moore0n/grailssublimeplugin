import sublime, sublime_plugin

class GrailsGoalsCommand(sublime_plugin.WindowCommand):
	wrkDir = None
	cmd = None
	
	def run(self,working_dir,cmd,grails_goals,env):
		self.cmd = [cmd]
		self.env = env
		self.wrkDir = working_dir
		self.window.show_input_panel("grails",grails_goals,self.on_done,None,None)

	def on_done(self, text):
		self.cmd += text.split(' ')
		self.window.run_command("exec",{"cmd":self.cmd, 'working_dir':self.wrkDir, "env":self.env})

class KillGrailsCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.window.run_command("exec",{"cmd":"taskkill /f /im java.exe"})